import logging
import time
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import RPi.GPIO as GPIO
import os
import board
import adafruit_dht
from pocketsphinx import Pocketsphinx
import threading
#usa el dht22 en el GPIO 17
dhtDevice = adafruit_dht.DHT22(board.D17)
cantpersonas = 0
# constantes
FNULL = open(os.devnull, 'w')
ENCENDER = "encender"
APAGAR = "apagar"
BANIO = "baño"
SALA = "sala"
HABITACION = "habitacion"
COCINA = "cocina"
PATIO = "patio"
CALOR = "calor"
FRIO = "frio"
HUMEDAD = "humedad"
TEMP = "temperatura"
CANT = "cantidad"
PERSONAS = "personas"
GPIOHAB = 18
GPIOINFR = 4
GPIOMOLI = 27
GPIOPATIO = 11
GPIOCOCINA = 9
GPIOSALA = 10
GPIOBANIO = 22
# pines
GPIO.setmode(GPIO.BCM)
# GPIO 18 - 12 - habitacion
GPIO.setup(GPIOHAB, GPIO.OUT)
GPIO.setup(GPIOMOLI, GPIO.OUT)
GPIO.setup(GPIOPATIO, GPIO.OUT)
GPIO.setup(GPIOCOCINA, GPIO.OUT)
GPIO.setup(GPIOSALA, GPIO.OUT)
GPIO.setup(GPIOBANIO, GPIO.OUT)
GPIO.setup(GPIOINFR, GPIO.IN)

reply_keyboard = [['encender sala', 'apagar sala', "encender baño"],
                  ['apagar baño', 'encender cocina', 'apagar cocina'],
                  ['encender patio', 'apagar patio', 'encender luces'],
                  ['apagar luces', 'temperatura', 'humedad'],
                  ['cantidad de personas', 'calor', 'frio'],
                  ['encender habitacion', 'apagar habitacion']]
markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

config = {
    'hmm': 'cmusphinx-es-5.2/model_parameters/voxforge_es_sphinx.cd_ptm_4000',
    'lm': 'train.lm',
    'dict': 'train.dict'
}
ps = Pocketsphinx(**config)
ps.decode(buffer_size=1048, no_search=False, full_utt=False)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)
logger = logging.getLogger(__name__)
# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.

def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hola, que tal?', reply_markup=markup)


def help(update, context):
    """Send a message when the command /help is issued."""
    res = "Ejemplo uso:\nConsejo\nQue es green it\nSabias que?"
    update.message.reply_text(res)


def echo(update, context):
    hipotesis = update.message.text
    if ENCENDER in hipotesis:
        if BANIO in hipotesis:
            GPIO.output(GPIOBANIO, GPIO.HIGH)
        elif COCINA in hipotesis:
            GPIO.output(GPIOCOCINA, GPIO.HIGH)
        elif SALA in hipotesis:
            GPIO.output(GPIOSALA, GPIO.HIGH)
        elif HABITACION in hipotesis:
            GPIO.output(GPIOHAB, GPIO.HIGH)
        elif PATIO in hipotesis:
            GPIO.output(GPIOPATIO, GPIO.HIGH)
    else:
        if APAGAR in hipotesis:
            if BANIO in hipotesis:
                GPIO.output(GPIOBANIO, GPIO.LOW)
            elif COCINA in hipotesis:
                GPIO.output(GPIOCOCINA, GPIO.LOW)
            elif SALA in hipotesis:
                GPIO.output(GPIOSALA, GPIO.LOW)
            elif HABITACION in hipotesis:
                GPIO.output(GPIOHAB, GPIO.LOW)
            elif PATIO in hipotesis:
                GPIO.output(GPIOPATIO, GPIO.LOW)
        else:
            if CALOR in hipotesis:
               GPIO.output(GPIOMOLI, GPIO.HIGH)
            if FRIO in hipotesis:
               GPIO.output(GPIOMOLI, GPIO.LOW)
            if HUMEDAD in hipotesis:
                try:
                    humidity = dhtDevice.humidity
                    update.message.reply_text(humidity)
                except RuntimeError:
                    pass
            if TEMP in hipotesis:
                try:
                    temperature_c = dhtDevice.temperature
                    update.message.reply_text(temperature_c)
                except RuntimeError:
                    pass
            if CANT in hipotesis or PERSONAS in hipotesis:
                global cantpersonas
                update.message.reply_text(f"El numero de personas que a entrado es: {cantpersonas}.")
    update.message.reply_text("ok")


def voice_handler(update, context):
    bot = context.bot
    file = bot.getFile(update.message.voice.file_id)
    file.download('/var/bot/voice.ogg')
    os.system("ffmpeg -i /var/bot/voice.ogg -acodec pcm_s16le -ac 1 -ar 16000 /var/bot/out.wav -y > /dev/null 2>&1")
    ps.decode(audio_file="/var/bot/out.wav")
    hipotesis = ps.hypothesis()
    if ENCENDER in hipotesis:
        if BANIO in hipotesis:
            GPIO.output(GPIOBANIO, GPIO.HIGH)
        elif COCINA in hipotesis:
            GPIO.output(GPIOCOCINA, GPIO.HIGH)
        elif SALA in hipotesis:
            GPIO.output(GPIOSALA, GPIO.HIGH)
        elif HABITACION in hipotesis:
            GPIO.output(GPIOHAB, GPIO.HIGH)
        elif PATIO in hipotesis:
            GPIO.output(GPIOPATIO, GPIO.HIGH)
    else:
        if APAGAR in hipotesis:
            if BANIO in hipotesis:
                GPIO.output(GPIOBANIO, GPIO.LOW)
            elif COCINA in hipotesis:
                GPIO.output(GPIOCOCINA, GPIO.LOW)
            elif SALA in hipotesis:
                GPIO.output(GPIOSALA, GPIO.LOW)
            elif HABITACION in hipotesis:
                GPIO.output(GPIOHAB, GPIO.LOW)
            elif PATIO in hipotesis:
                GPIO.output(GPIOPATIO, GPIO.LOW)
        else:
            if CALOR in hipotesis:
               GPIO.output(GPIOMOLI, GPIO.HIGH)
            if FRIO in hipotesis:
               GPIO.output(GPIOMOLI, GPIO.LOW)
            if HUMEDAD in hipotesis:
                try:
                    humidity = dhtDevice.humidity
                    update.message.reply_text(humidity)
                except RuntimeError:
                    pass
            if TEMP in hipotesis:
                try:
                    temperature_c = dhtDevice.temperature
                    update.message.reply_text(temperature_c)
                except RuntimeError:
                    pass
            if CANT in hipotesis or PERSONAS in hipotesis:
                global cantpersonas
                update.message.reply_text(f"El numero de personas que a entrado es: {cantpersonas}.")
    update.message.reply_text("ok")


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)
# leer sensor infrarrojo
def readinfra():
    while True:
        if GPIO.input(GPIOINFR) == True:
           global cantpersonas
           cantpersonas+=1
        time.sleep(2)
hinfra = threading.Thread(target=readinfra)
hinfra.start()
def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("995623658:AAH_xX4hSZxF-rB6YtARGS6Zu6XMBrvFnrQ", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(MessageHandler(Filters.voice, voice_handler))
    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
